﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Order
{
    /// <summary>
    /// Enum for TransactionStatusCodes
    /// With inspiration from https://support.bigcommerce.com/s/article/Order-Statuses#order-statuses
    /// </summary>
    public enum TransactionStatusCode
    {
        NotSet = 0,
        Initializing = 50,
        Pending = 100,
        AwaitingPayment = 200,
        AwaitingFulfillment = 300,
        AwaitingShipment = 400,
        PartiallyShipped = 500,
        Shipped = 600,
        Completed = 700,
        Cancelled = 800,
        PartiallyRefunded = 900,
        Refunded = 1000
    }
}
