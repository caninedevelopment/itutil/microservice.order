﻿namespace Microservice.Order.DTO
{
    using System;
    public class GetByOrderIdDTO
    {
        public Guid Id { get; set; }
    }
}
