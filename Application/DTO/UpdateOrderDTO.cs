﻿namespace Microservice.Order.DTO
{
    using ITUtil.Common.Base;
    using MongoDB.Bson.Serialization.Attributes;
    using System.Collections.Generic;

    /// <summary>
    /// Order.
    /// </summary>
    public class UpdateOrderDTO : InsertOrderDTO
    {
        public TransactionStatusCode TransactionStatusCode { get; set; }
    }
}
