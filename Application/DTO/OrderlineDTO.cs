﻿namespace Microservice.Order.DTO
{
    using ITUtil.Common.Base;

    /// <summary>
    /// Orderline.
    /// </summary>
    public class OrderlineDTO
    {
        /// <summary>
        /// Gets or sets ExternalProduct. never changing Id NOT SKU.
        /// </summary>
        public ExternalReference ExternalProduct { get; set; }

        /// <summary>
        /// Gets or sets unitPriceExVat.
        /// </summary>
        public decimal UnitPriceExVat { get; set; }

        /// <summary>
        /// Gets or sets quantity.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets vat.
        /// </summary>
        public int Vat { get; set; }

        /// <summary>
        /// Gets or sets Variation.
        /// </summary>
        public string Variation { get; set; }

        public OrderlineDTO() { }
        public OrderlineDTO(Database.Collections.Orderline orderline) 
        {
            this.ExternalProduct = orderline.ExternalProduct;
            this.Quantity = orderline.Quantity;
            this.UnitPriceExVat = orderline.UnitPriceExVat;
            this.Variation = orderline.Variation;
            this.Vat = orderline.Vat;
        }
    }
}
