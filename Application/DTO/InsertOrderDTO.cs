﻿namespace Microservice.Order.DTO
{
    using ITUtil.Common.Base;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Order.
    /// </summary>
    public class InsertOrderDTO : BaseOrder
    {
        /// <summary>
        /// Gets or sets ExternalUser.
        /// </summary>
        public ExternalReference ExternalUser { get; set; }


        /// <summary>
        /// Gets or sets PaymentDate.
        /// </summary>
        public DateTime PaymentDate { get; set; }

        /// <summary>
        /// Gets or sets ShippedDate.
        /// </summary>
        public DateTime ShippedDate { get; set; } // Todo: Should this be in the Orderline class to enable different shipping dates?

        /// <summary>
        /// Gets or sets BillingAddress.
        /// </summary>
        public AddressDTO BillingAddress { get; set; }

        /// <summary>
        /// Gets or sets ShippingAddress.
        /// </summary>
        public AddressDTO ShippingAddress { get; set; }

    }

}
