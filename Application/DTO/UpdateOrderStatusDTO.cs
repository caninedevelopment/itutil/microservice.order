﻿namespace Microservice.Order.DTO
{
    using System;

    public class UpdateOrderStatusDTO
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets TransactionStatusCode.
        /// </summary>
        public TransactionStatusCode TransactionStatusCode { get; set; }
    }
}
