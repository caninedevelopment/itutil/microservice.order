﻿namespace Microservice.Order.DTO
{
    using ITUtil.Common.Base;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class BaseOrder
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets Orderlines.
        /// </summary>
        public List<OrderlineDTO> Orderlines { get; set; }
    }


    /// <summary>
    /// Order.
    /// </summary>
    public class GetOrderDTO : BaseOrder
    {
        /// <summary>
        /// Gets or sets ExternalUser.
        /// </summary>
        public ExternalReference ExternalUser { get; set; }

        /// <summary>
        /// Gets or sets TransactionStatusCode.
        /// </summary>
        public TransactionStatusCode TransactionStatusCode { get; set; }

        /// <summary>
        /// Gets or sets TransactionStatusMessage.
        /// </summary>
        public string TransactionStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets CreatedDate.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets PaymentDate.
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// Gets or sets ShippedDate.
        /// </summary>
        public DateTime? ShippedDate { get; set; } // Todo: Should this be in the Orderline class to enable different shipping dates?

        /// <summary>
        /// Gets or sets BillingAddress.
        /// </summary>
        public AddressDTO BillingAddress { get; set; }

        /// <summary>
        /// Gets or sets ShippingAddress.
        /// </summary>
        public AddressDTO ShippingAddress { get; set; }

        public GetOrderDTO() { }
        public GetOrderDTO(Database.Collections.Order order) 
        {
            this.BillingAddress = new AddressDTO(order.BillingAddress);
            this.CreatedDate = order.CreatedDate;
            this.ExternalUser = order.ExternalUser;
            this.Id = order.Id;
            this.Orderlines = order.Orderlines.Select(orderline => new DTO.OrderlineDTO(orderline)).ToList();
            this.PaymentDate = order.PaymentDate;
            this.ShippedDate = order.ShippedDate;
            this.ShippingAddress = new AddressDTO(order.ShippingAddress);
            this.TransactionStatusCode = order.TransactionStatusCode;
        }
    }

}
