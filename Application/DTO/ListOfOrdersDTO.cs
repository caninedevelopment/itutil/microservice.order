﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Order.DTO
{
    public class ListOfOrdersDTO
    {
        public List<DTO.GetOrderDTO> list { get; set; }
    }
}
