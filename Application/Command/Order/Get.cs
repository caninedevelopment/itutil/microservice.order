﻿
namespace Microservice.Order.Command.Order
{
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;
    public class Get : GetCommand<DTO.GetByOrderIdDTO, DTO.GetOrderDTO>
    {
        public Get() : base("Get order by id") { }

        public override DTO.GetOrderDTO Execute(DTO.GetByOrderIdDTO input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }
            var order = DataContext.Instance.GetOrderById(input.Id);
            if (order == null)
            {
                throw new ElementDoesNotExistException("Order does not exist", input.Id.ToString());
            }
            return new DTO.GetOrderDTO(order);
        }
    }
}