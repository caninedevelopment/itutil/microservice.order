﻿namespace Microservice.Order.Command.Order
{
    using ITUtil.Common.Command;
    using System.Linq;

    public class GetAll : GetCommand<object, DTO.ListOfOrdersDTO>
    {
        public GetAll() : base("Get all orders") { }

        public override DTO.ListOfOrdersDTO Execute(object input = null) // Todo Maybe write another overload with no arguments in the GetCommand?
        {
            DTO.ListOfOrdersDTO orders = new DTO.ListOfOrdersDTO();
            orders.list = DataContext.Instance.GetOrders().Select(order => new DTO.GetOrderDTO(order)).ToList();
            return orders;
        }
    }
}