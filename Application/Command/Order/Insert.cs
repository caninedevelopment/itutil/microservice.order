﻿
namespace Microservice.Order.Command.Order
{
    using System;
    using System.Linq;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Insert : InsertCommand<DTO.InsertOrderDTO>
    {
        public Insert() : base("Inserts an order, Id and Orderlines are required, the Id must be unique otherwise an already exists exception will be raised. Please note: an empty list of orderlines will throw an error.")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.InsertOrderDTO input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }

            if (input.Orderlines == null || input.Orderlines.Any() == false)
            {
                throw new NullOrDefaultException("Orderlines were not provided", "Orderlines");
            }

            var order = DataContext.Instance.GetOrderById(input.Id);
            if (order != null)
            {
                throw new ElementAlreadyExistsException("Order already exists", input.Id.ToString());
            }
            order = new Database.Collections.Order()
            {
                Id = input.Id,
                ExternalUser = input.ExternalUser,
                TransactionStatusCode = TransactionStatusCode.Initializing,
                CreatedDate = DateTime.UtcNow,
                PaymentDate = input.PaymentDate,
                ShippedDate = input.ShippedDate,
                BillingAddress = input.BillingAddress != null ? new Database.Collections.Address(input.BillingAddress) : new Database.Collections.Address(input.ShippingAddress),
                ShippingAddress = input.ShippingAddress != null ? new Database.Collections.Address(input.ShippingAddress) : new Database.Collections.Address(input.BillingAddress),
                Orderlines = input.Orderlines.Select(orderline => new Database.Collections.Orderline(orderline)).ToList()
            };
            DataContext.Instance.InsertOrder(order);
        }
    }
}
