﻿namespace Microservice.Order.Command.Order
{
    using ITUtil.Common.Command;
    using System.Linq;

    public class GetAllByUserId : GetCommand<DTO.GetByUserId, DTO.ListOfOrdersDTO>
    {
        public GetAllByUserId() : base("Get orders by userId") { }

        public override DTO.ListOfOrdersDTO Execute(DTO.GetByUserId input)
        {
            if (string.IsNullOrEmpty(input.ExternalId))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("ExternalId was not provided", "ExternalId");
            }
            DTO.ListOfOrdersDTO orders = new DTO.ListOfOrdersDTO();
            orders.list = DataContext.Instance.GetOrdersByUserId(input.ExternalId).Select(order => new DTO.GetOrderDTO(order)).ToList();
            return orders;
        }
    }
}