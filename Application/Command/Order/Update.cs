﻿
namespace Microservice.Order.Command.Order
{
    using System;
    using System.Linq;
    using ITUtil.Common.Command;
    public class Update : UpdateCommand<DTO.UpdateOrderDTO>
    {
        public Update() : base("Update an order all properties are required except BillingAddress/ShippingAddress which will default to each other if only one is provided. TransactionStatusCode must be different than 0/NotSet")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.UpdateOrderDTO input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }
            if (input.TransactionStatusCode == TransactionStatusCode.NotSet)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("TransactionStatusCode was not provided", "TransactionStatusCode");
            }
            if (input.ExternalUser == null)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("ExternalUser was not provided", "ExternalUser");
            }
            if (input.BillingAddress == null && input.ShippingAddress == null)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Either BillingAddress or ShippingAddress must be provided", "BillingAddress/ShippingAddress");
            }
            if (input.Orderlines == null || input.Orderlines.Any() == false)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Orderlines were not provided", "Orderlines");
            }


            var order = DataContext.Instance.GetOrderById(input.Id);
            if (order == null)
            {
                throw new ITUtil.Common.Base.ElementDoesNotExistException("Order does not exist", input.Id.ToString());
            }
            order.ExternalUser = input.ExternalUser;
            order.TransactionStatusCode = input.TransactionStatusCode;
            order.PaymentDate = input.PaymentDate;
            order.ShippedDate = input.ShippedDate;
            order.BillingAddress = input.BillingAddress != null ? new Database.Collections.Address(input.BillingAddress) : new Database.Collections.Address(input.ShippingAddress);
            order.ShippingAddress = input.ShippingAddress != null ? new Database.Collections.Address(input.ShippingAddress) : new Database.Collections.Address(input.BillingAddress);
            order.Orderlines = input.Orderlines.Select(orderline => new Database.Collections.Orderline(orderline)).ToList();
            DataContext.Instance.UpdateOrder(order);
        }
    }
}
