﻿
namespace Microservice.Order.Command.Order
{
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Namespace : BaseNamespace
    {
        internal static Claim IsAdmin = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is administrator, i.e. can change user rights") }, key = "Monosoft.Order.isAdmin" };
        internal static Claim IsCustomer = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is a customer, i.e. can create own subscriptions") }, key = "Monosoft.Order.isCustomer" };
        public Namespace() : base("Order", new ProgramVersion("1.0.0.0"))
        {
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new Insert(),
                new Get(),
                new GetAll(),
                new GetAllByUserId(),
                new Update(),
                new UpdateStatus(),
                new Delete()
            });
        }

    }
}
