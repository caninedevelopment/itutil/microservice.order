﻿namespace Microservice.Order.Command.Order
{
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;
    public class Delete : DeleteCommand<DTO.GetByOrderIdDTO>
    {
        public Delete() : base("Delete an order by it Id")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.GetByOrderIdDTO input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }
            var order = DataContext.Instance.GetOrderById(input.Id);
            if (order == null)
            {
                throw new ElementDoesNotExistException("Order does not exist", input.Id.ToString());
            }
            DataContext.Instance.DeleteOrder(input.Id);
        }
    }
}