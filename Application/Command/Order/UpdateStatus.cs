﻿
namespace Microservice.Order.Command.Order
{
    using System;
    using System.Linq;
    using ITUtil.Common.Command;
    public class UpdateStatus : UpdateCommand<DTO.UpdateOrderStatusDTO>
    {
        public UpdateStatus() : base("Update an order status")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.UpdateOrderStatusDTO input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }

            var order = DataContext.Instance.GetOrderById(input.Id);
            if (order == null)
            {
                throw new ITUtil.Common.Base.ElementDoesNotExistException("Order does not exist", input.Id.ToString());
            }
            order.TransactionStatusCode = input.TransactionStatusCode;
            DataContext.Instance.UpdateOrder(order);
        }
    }
}
