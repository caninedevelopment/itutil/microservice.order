﻿namespace Microservice.Order.Database.Collections
{
    using ITUtil.Common.Base;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Collections.Generic;



    /// <summary>
    /// Order.
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [BsonId] //converts the Property to _id. Auto generates Guid.
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets ExternalUser.
        /// </summary>
        public ExternalReference ExternalUser { get; set; }

        /// <summary>
        /// Gets or sets TransactionStatusCode.
        /// </summary>
        public TransactionStatusCode TransactionStatusCode { get; set; }

        /// <summary>
        /// Gets or sets TransactionStatusMessage.
        /// </summary>
        public string TransactionStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets CreatedDate.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets PaymentDate.
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// Gets or sets ShippedDate.
        /// </summary>
        public DateTime? ShippedDate { get; set; } // Todo: Should this be in the Orderline class to enable different shipping dates?

        /// <summary>
        /// Gets or sets BillingAddress.
        /// </summary>
        public Address BillingAddress { get; set; }

        /// <summary>
        /// Gets or sets ShippingAddress.
        /// </summary>
        public Address ShippingAddress { get; set; }

        /// <summary>
        /// Gets or sets Orderlines.
        /// </summary>
        public List<Orderline> Orderlines { get; set; }
    }

}
