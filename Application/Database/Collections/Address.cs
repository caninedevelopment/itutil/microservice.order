﻿
namespace Microservice.Order.Database.Collections
{
    /// <summary>
    /// Address.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Gets or sets Fullname.
        /// </summary>
        public string Fullname { get; set; }

        /// <summary>
        /// Gets or sets Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets Company.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets AdressLine1.
        /// </summary>
        public string AdressLine1 { get; set; }

        /// <summary>
        /// Gets or sets AdressLine2.
        /// </summary>
        public string AdressLine2 { get; set; }

        /// <summary>
        /// Gets or sets Zip.
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets Country.
        /// </summary>
        public string Country { get; set; } // Todo: Should this be a enum with country codes?

        public Address() { }

        public Address(DTO.AddressDTO address)
        {
            Fullname = address.Fullname;
            Phone = address.Phone;
            Company = address.Company;
            AdressLine1 = address.AdressLine1;
            AdressLine2 = address.AdressLine2;
            Zip = address.Zip;
            City = address.City;
            Country = address.Country;
        }
    }
}
