﻿namespace Microservice.Order
{
    using System;
    using System.Collections.Generic;
    using MongoDB.Bson.Serialization.Conventions;
    using MongoDB.Driver;

    /// <summary>
    /// Datacontext for the orderdatabase.
    /// </summary>
    public class DataContext
    {
        private readonly string dbName = "orderDatabase";
        private readonly string collectionName = "order";
        private readonly IMongoDatabase mongoDb;

        public static DataContext Instance = new DataContext();
        /// <summary>
        /// Initializes a new instance of the <see cref="DataContext"/> class.
        /// </summary>
        public DataContext()
        {
            var customConventions = new ConventionPack {
                new IgnoreExtraElementsConvention(true), // Ignore the extra fields that a document has when compared to the provided DTO on Deserialization.  
                new IgnoreIfDefaultConvention(true) // Ignore the fields with default value on Serialization. Keeps the document smaller.
            };
            ConventionRegistry.Register("CustomConventions", customConventions, type => true); // how to handle different encounters during serialization & deserialization.
            var settings = ITUtil.Common.Config.GlobalRessources.getConfig();
            if (settings == null)
            {
                throw new Exception("GlobalRessource settings were not provided");
            }
            var NOSQL = settings.GetSetting<ITUtil.Common.Config.NoSQLSettings>();
            var client = new MongoClient(NOSQL.ConnectionString());
            mongoDb = client.GetDatabase(NOSQL.GetFullDBName(dbName)); // automatically creates a database if it doesn't exists
        }
        public void InsertOrder(Database.Collections.Order document) 
        {
            var orderCollection = this.mongoDb.GetCollection<Database.Collections.Order>(collectionName);
            orderCollection.InsertOne(document);
        }

        public List<Database.Collections.Order> GetOrders()
        {
            var orderCollection = mongoDb.GetCollection<Database.Collections.Order>(collectionName);
            return orderCollection.Find(Order => true).ToList();
        }
        public List<Database.Collections.Order> GetOrdersByUserId(string userId) // make more dynamic? Aka GetOrdersBy
        {
            var orderCollection = mongoDb.GetCollection<Database.Collections.Order>(collectionName);
            var filter = Builders<Database.Collections.Order>.Filter.Eq("ExternalUser.Id", userId);
            return orderCollection.Find(filter).ToList();
        }
        public Database.Collections.Order GetOrderById(Guid id)
        {
            var orderCollection = mongoDb.GetCollection<Database.Collections.Order>(collectionName);
            var filter = Builders<Database.Collections.Order>.Filter.Eq("Id", id);
            return orderCollection.Find(filter).FirstOrDefault();
        }

        public void UpdateOrder(Database.Collections.Order order)
        {
            var orderCollection = mongoDb.GetCollection<Database.Collections.Order>(collectionName);
            var filter = Builders<Database.Collections.Order>.Filter.Eq("Id", order.Id);
            orderCollection.ReplaceOne(
                filter,
                order
            );
        }

        public void DeleteOrder(Guid id)
        {
            var orderCollection = mongoDb.GetCollection<Database.Collections.Order>(collectionName);
            var filter = Builders<Database.Collections.Order>.Filter.Eq("Id", id);
            orderCollection.DeleteOne(filter);
        }

    }
}
