FROM mcr.microsoft.com/dotnet/core/runtime:3.0

COPY Application/bin/Release/netcoreapp3.0/publish/ /App

WORKDIR /App

ENTRYPOINT ["dotnet", "Application.Order.dll"]