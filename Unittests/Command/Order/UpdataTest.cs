﻿namespace Unittests.Order.Command.Order
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Order;
    using Microservice.Order.Command.Order;
    using Microservice.Order.DTO;
    using ITUtil.Common.Base;

    [TestFixture]
    public class UpdataTest
    {
        [Test]
        public void UpdateOne_Success()
        {
            Insert insert = new Insert();
            Update update = new Update();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = null,
                Orderlines = orderLines
            };
            insert.Execute(orderForInsert);

            var orderForupdate = new Microservice.Order.DTO.UpdateOrderDTO()
            {
                Id = orderForInsert.Id,
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                TransactionStatusCode = Microservice.Order.TransactionStatusCode.Pending,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };
            update.Execute(orderForupdate);
        }
        [Test]
        public void UpdateWithNoId_Error()
        {
            Update update = new Update();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");
            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForupdate = new Microservice.Order.DTO.UpdateOrderDTO()
            {
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                TransactionStatusCode = Microservice.Order.TransactionStatusCode.Pending,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };

            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(orderForupdate));
        }
        [Test]
        public void UpdateWithNoTransactionStatusCode_Error()
        {
            Update update = new Update();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");
            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForupdate = new Microservice.Order.DTO.UpdateOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };

            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(orderForupdate));
        }
        [Test]
        public void UpdateWithNoExternalUser_Error()
        {
            Update update = new Update();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");
            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForupdate = new Microservice.Order.DTO.UpdateOrderDTO()
            {
                Id = Guid.NewGuid(),
                PaymentDate = DateTime.MinValue,
                TransactionStatusCode = Microservice.Order.TransactionStatusCode.Pending,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };

            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(orderForupdate));
        }
        [Test]
        public void UpdateWithNoBillingAndShippingAddress_Error()
        {
            Update update = new Update();
            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForupdate = new Microservice.Order.DTO.UpdateOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                TransactionStatusCode = Microservice.Order.TransactionStatusCode.Pending,
                ShippedDate = DateTime.MinValue,
                Orderlines = orderLines
            };

            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(orderForupdate));
        }
        [Test]
        public void UpdateWithNoOrderlines_Error()
        {
            Update update = new Update();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderForupdate = new Microservice.Order.DTO.UpdateOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                TransactionStatusCode = Microservice.Order.TransactionStatusCode.Pending,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
            };

            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(orderForupdate));
        }
    }
}
