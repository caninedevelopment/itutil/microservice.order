﻿namespace Unittests.Order.Command.Order
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Order;
    using Microservice.Order.Command.Order;
    using Microservice.Order.DTO;
    using ITUtil.Common.Base;

    [TestFixture]
    public class DeleteTest
    {
        [Test]
        public void DeleteOne_Success()
        {
            Delete delete = new Delete();

            #region preparation
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };

            GetByOrderIdDTO byid = new GetByOrderIdDTO();
            byid.Id = orderForInsert.Id;
            insert.Execute(orderForInsert);
            #endregion

            delete.Execute(byid);
        }
        [Test]
        public void DeleteOne_ElementDoesNotExistException()
        {
            Delete delete = new Delete();

            GetByOrderIdDTO byid = new GetByOrderIdDTO();
            byid.Id = Guid.NewGuid();
            Assert.Throws<ITUtil.Common.Base.ElementDoesNotExistException>(() => delete.Execute(byid));
        }
        [Test]
        public void DeleteOne_WithNoId()
        {
            Delete delete = new Delete();

            GetByOrderIdDTO byid = new GetByOrderIdDTO();
            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => delete.Execute(byid));
        }
    }
}
