﻿namespace Unittests.Order.Command.Order
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Order;
    using Microservice.Order.Command.Order;
    using Microservice.Order.DTO;
    using ITUtil.Common.Base;

    [TestFixture]
    public class GetAllTest
    {
        [Test]
        public void GetAll_Success()
        {
            GetAll getall = new GetAll();
            Insert insert = new Insert();

            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr4", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };


            var beforinsert = getall.Execute();
            insert.Execute(orderForInsert);
            var afterinsert = getall.Execute();

            Assert.IsTrue(afterinsert.list.Count > beforinsert.list.Count);
        }

    }
}
