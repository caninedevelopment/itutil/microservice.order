﻿namespace Unittests.Order.Command.Order
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Order;
    using Microservice.Order.Command.Order;
    using Microservice.Order.DTO;
    using ITUtil.Common.Base;

    [TestFixture]
    public class GetTest
    {
        [Test]
        public void GetOne_Success()
        {
            Get get = new Get();
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr3", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };
            insert.Execute(orderForInsert);
            GetByOrderIdDTO getByOrderId = new GetByOrderIdDTO();
            getByOrderId.Id = orderForInsert.Id;

            var res = get.Execute(getByOrderId);
            Assert.AreEqual(orderForInsert.Id, res.Id);
            Assert.AreEqual(orderForInsert.ExternalUser.Id, res.ExternalUser.Id);
            Assert.AreEqual(orderForInsert.PaymentDate, res.PaymentDate); 
            Assert.AreEqual(orderForInsert.ShippedDate, res.ShippedDate);
            Assert.AreEqual(orderForInsert.BillingAddress.Fullname, res.BillingAddress.Fullname);
            Assert.AreEqual(orderForInsert.BillingAddress.Phone, res.BillingAddress.Phone);
            Assert.AreEqual(orderForInsert.BillingAddress.Company, res.BillingAddress.Company);
            Assert.AreEqual(orderForInsert.BillingAddress.AdressLine1, res.BillingAddress.AdressLine1);
            Assert.AreEqual(orderForInsert.BillingAddress.Zip, res.BillingAddress.Zip);
            Assert.AreEqual(orderForInsert.BillingAddress.City, res.BillingAddress.City);
            Assert.AreEqual(orderForInsert.BillingAddress.Country, res.BillingAddress.Country);
            Assert.AreEqual(orderForInsert.ShippingAddress.Fullname, res.ShippingAddress.Fullname);
            Assert.AreEqual(orderForInsert.ShippingAddress.Phone, res.ShippingAddress.Phone);
            Assert.AreEqual(orderForInsert.ShippingAddress.Company, res.ShippingAddress.Company);
            Assert.AreEqual(orderForInsert.ShippingAddress.AdressLine1, res.ShippingAddress.AdressLine1);
            Assert.AreEqual(orderForInsert.ShippingAddress.Zip, res.ShippingAddress.Zip);
            Assert.AreEqual(orderForInsert.ShippingAddress.City, res.ShippingAddress.City);
            Assert.AreEqual(orderForInsert.ShippingAddress.Country, res.ShippingAddress.Country);
            for (int i = 0; i < orderForInsert.Orderlines.Count; i++)
            {
                Assert.AreEqual(orderForInsert.Orderlines[i].ExternalProduct.Id, res.Orderlines[i].ExternalProduct.Id);
                Assert.AreEqual(orderForInsert.Orderlines[i].UnitPriceExVat, res.Orderlines[i].UnitPriceExVat);
                Assert.AreEqual(orderForInsert.Orderlines[i].Quantity, res.Orderlines[i].Quantity);
                Assert.AreEqual(orderForInsert.Orderlines[i].Variation, res.Orderlines[i].Variation);
                Assert.AreEqual(orderForInsert.Orderlines[i].Vat, res.Orderlines[i].Vat);
            }

        }
    }
}
