﻿
namespace Unittests.Order.Command.Order
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Order.Command.Order;
    using Microservice.Order.DTO;
    using ITUtil.Common.Base;
    using Microservice.Order;

    [TestFixture]
    public class InsertTests
    {
        [Test]
        public void InsertOne_Success()
        {
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };
            insert.Execute(orderForInsert);
        }

        [Test]
        public void InsertOneWithMissingId_Error()
        {
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = null,
                Orderlines = orderLines
            };
            var ex = Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => insert.Execute(orderForInsert));
            Assert.That(ex.Message, Is.EqualTo("Id was not provided"));
        }

        [Test]
        public void InsertOneWithMissingOrderlines_Error()
        {
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
            };
            var ex = Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => insert.Execute(orderForInsert));
            Assert.That(ex.Message, Is.EqualTo("Orderlines were not provided"));
        }

        [Test]
        public void InsertOneWithNewListOrderlines_Error()
        {
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = new List<OrderlineDTO>(),
            };
            var ex = Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => insert.Execute(orderForInsert));
            Assert.That(ex.Message, Is.EqualTo("Orderlines were not provided"));
        }

        [Test]
        public void InsertOneWhereOrderAlreadyExists_Error()
        {
            #region preparation
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "", "1234", "CityName", "CountryName");
            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };
            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = null,
                Orderlines = orderLines
            };
            insert.Execute(orderForInsert);
            #endregion
            var orderThatAlreadyExists = orderForInsert;
            ElementAlreadyExistsException ex = Assert.Throws<ITUtil.Common.Base.ElementAlreadyExistsException>(() => insert.Execute(orderThatAlreadyExists));
            Assert.That(ex.Message, Is.EqualTo("Order already exists"));
        }    
    }
}
