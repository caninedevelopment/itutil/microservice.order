﻿namespace Unittests.Order.Command.Order
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Order;
    using Microservice.Order.Command.Order;
    using Microservice.Order.DTO;
    using ITUtil.Common.Base;

    [TestFixture]
    public class GetByUserIdTests
    {
        [Test]
        public void GetByUserId_Success()
        {
            #region Preparation
            Insert insert = new Insert();
            Microservice.Order.DTO.AddressDTO billingAddress = new Microservice.Order.DTO.AddressDTO("John", "Doe", "John Doe APS", "Address1", "Address2", "1234", "CityName", "CountryName");

            var orderLines = new List<Microservice.Order.DTO.OrderlineDTO>()
            {
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product1", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 200,
                    Quantity = 2,
                    Vat = 25,
                    Variation = "Blue"
                },
                new OrderlineDTO()
                {
                    ExternalProduct = new ExternalReference("Product2", new ExternalReference.PathDefinition("Microservice.Product", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                    UnitPriceExVat = 299,
                    Quantity = 4,
                    Vat = 25,
                    Variation = "Red"
                }
            };

            var orderForInsert = new Microservice.Order.DTO.InsertOrderDTO()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr2", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                PaymentDate = DateTime.MinValue,
                ShippedDate = DateTime.MinValue,
                BillingAddress = billingAddress,
                ShippingAddress = billingAddress,
                Orderlines = orderLines
            };
            insert.Execute(orderForInsert);
            #endregion


            GetAllByUserId getByUserId = new GetAllByUserId();
            GetByUserId byUserId = new GetByUserId();
            byUserId.ExternalId = orderForInsert.ExternalUser.Id;
            getByUserId.Execute(byUserId);
        }
        [Test]
        public void GetByUserIdWithMissingId_Error()
        {
            GetAllByUserId getByUserId = new GetAllByUserId();
            GetByUserId byUserId = new GetByUserId();
            var ex = Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => getByUserId.Execute(byUserId));
            Assert.That(ex.Message, Is.EqualTo("ExternalId was not provided"));
        }
    }
}
